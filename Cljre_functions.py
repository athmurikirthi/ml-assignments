
# coding: utf-8

# In[30]:

def filter(perd, lst):
    filt = []
    for e in lst:
        if perd(e):
            filt.append(e)
    return filt    
        


# In[113]:

filter(even , [10,3,2,4,6,8,9])


# In[36]:

def even(n):
        if n%2 == 0:
            return n
        else:
            return False


# In[106]:

even(2)


# In[108]:

def take_while(perd, lst):
    res = []
    for e in lst:
        if perd(e):
            res.append(e)
        else:
            return res
    return res   
        


# In[111]:

take_while(even ,[2,4,6,3,1,7,8])


# In[38]:

def map(f,lst):
    mapped = []
    for e in lst:
        mapped.append(f(e))
    return mapped


# In[40]:

def inc(x):
    x=x+1
    return x


# In[41]:

inc(2)


# In[42]:

map(inc ,[1,2,3,4])


# In[68]:

def distinct(lst):                                               
    res = []
    for e in lst:
        if e not in res:
            res.append(e)
    return sorted(res)

        


# In[69]:

distinct([1,1,2,2,4,4,5,6,3,3,7,8,9,9,10,11,22,3,4,5,6,7,1,2,1415])


# In[87]:

def reduce(x,y):
    def reduced(lst):
        result = y
        for e in lst:
            result = x(result, e)
        return result
    return reduced


# In[85]:

reduce(add,2)([1,2,3,4])


# In[74]:

def add(x,y):
    return x+y
add(1,2)


# In[121]:

def dedupe(lst):
    res=[]
    for e in range(len(lst)):
        if lst[e] != lst[e-1]:
            res.append(lst[e])
    return res   


# In[122]:

dedupe([1,1,1,2,2,3,4,5,1,1,6,2])


# In[104]:

def take(n,lst):
    res=[]
    for e in range(n):
        res.append(lst[e])
    return res  
    


# In[105]:

take(3,[1,2,3,4,5,6,7,8,9])


# In[117]:

def reverse(lst):
    res = []
    for e in range(1,len(lst)+1):
        res.append(lst[-e])
    return res 
        
reverse([1,2,3,4])


# In[123]:

def concat(lst1,lst2):
    res = lst1
    for i in lst2 :
        res.append(i)
    return res
    


# In[124]:

concat([1,2,3,4],[4,5,6])


# In[1]:

def frequency(lst):
    freq={ }
    for e in lst:
        freq[e]=freq.get(e,0)+1
    return freq


# In[4]:

frequency([1,2,3,1,1,1,3,3,3,5,5,5])


# In[30]:

def index(lst):
    res = {}
    for i in range(len(lst)):
        res[lst[i]]= concat1(res.get(lst[i],[]),i)
    return res
    


# In[31]:

index([1,2,3,1,1,1,3,3,3,5,5,5])


# In[26]:

def concat1(lst1,b):
    res = []
    for i in lst1 :
        res.append(i)
    res.append(b)
    return res


# In[32]:

concat1([1,2,3],4)


# In[3]:

def flatten(lst):
    res=[]
    for i in lst:
        for e in i:
            res.append(e)
    return res 


# In[13]:

flatten([[1,2,3],[2,5]])


# In[88]:

def interleave1(lst1,lst2):
    res=[]
    while lst1 and lst2:
        res.append(lst1.pop(0))
        res.append(lst2.pop(0))
    
    return res


# In[91]:

interleave1([1, 2, 5],[3])


# In[ ]:




# In[ ]:



