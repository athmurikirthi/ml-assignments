
for countdown in 1,2,3,4,5,"hey":
    print(countdown)
#1
#2
#3
#4
#5
#hey
#===============================
cliches = ["At the end of the day",
           "Having said that",
           "The fact of the matter is",
           "Be that as it may",
           "The bottom line is",
           "If you will"]
print(cliches[1])
#Having said that
#========================
a
a=9
print(a)
#9
#===============
b
b=a
print(b)
#9
#==================
type(a)
#int
#=================
b
type(b)
#int
#====================
58
type(58)
#int
#===============
10.5
type(10.5)
#float
#====================
type('abd')
#str
#====================
+123
#123
#====================
-122
#-122
#====================
5+9
#14
#====================
100-7
#93
#====================
4-10
#-6
#====================
5+9+3
#17
#====================    
5+9    +     3
#17
#====================
6*7
#42
#====================
6*7*2*3
#252
#====================
9/5
#1.8
#====================
9//5
#1
#====================
5/0
---------------------------------------------------------------------------
ZeroDivisionError                         Traceback (most recent call last)
<ipython-input-30-67a69f72677d> in <module>()
----> 1 5/0

ZeroDivisionError: division by zero
#====================
a=95
a
#95
#====================
a-3
#92
#====================
temp
a
temp=a-3
temp
#92
#====================
a
a=95
a-=3
a
#92
#====================
a
#92
#====================
a=92
a/=3
a
#30.666666666666668
#====================
9%5
#4
#====================
divmod(9,5)
#(1, 4)
#====================
2+3*4
#14
#====================
2+(3*4)
#14
#====================
10
#10
#====================
0b1110
#14
#====================
0o10
#8
#===================
0xa
#10
#====================
int(True)
#1
#====================
int(False)
#0
#====================
int(-99)
#-99
#====================
int('-99')
#-99
#====================
int('99 bottles of beer on the wall')
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-99-fd87007a62a6> in <module>()
----> 1 int('99 bottles of beer on the wall')

ValueError: invalid literal for int() with base 10: '99 bottles of beer on the wall'
#====================
True+9
#10
#====================
False-9
#-9
#====================
googol=10**100
googol
#10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
#====================
float(True)
#1.0
#====================
False
float(False)
#0.0
#====================
'snap'
#'snap'
#====================
"crackle"
#'crackle'
#====================
 "'Nay,' said the naysayer." 
#"'Nay,' said the naysayer."
#====================
'''boom'''
#'boom'
#====================
"""boom"""
#'boom'
#====================
poem = 'There was a young lady of Norway,
        'There was a young lady of Norway'
  File "<ipython-input-128-82e0418f205a>", line 1
    poem = 'There was a young lady of Norway,
                                             ^
SyntaxError: EOL while scanning string literal
#====================
poem='''I do not like thee, Doctor Fell. 
...The reason why, I cannot tell. 
...But this I know, and know full well:
...I do not like thee, Doctor Fell. '''
poem='''I do not like thee, Doctor Fell. 
...The reason why, I cannot tell. 
...But this I know, and know full well:
...I do not like thee, Doctor Fell. '''
poem
#'I do not like thee, Doctor Fell. \n...The reason why, I cannot tell. \n...But this I know, and know full well:\n...I do not like thee, Doctor Fell. '
#====================
poem='''I do not like thee, Doctor Fell. 
...The reason why, I cannot tell. 
...But this I know, and know full well:
...I do not like thee, Doctor Fell. '''
print(poem)
#I do not like thee, Doctor Fell. 
#...The reason why, I cannot tell. 
#...But this I know, and know full well:
#...I do not like thee, Doctor Fell. 
#====================
print(99,'bottle','would be enough')
#99 bottle would be enough
#====================
''
#''
#====================
""
#''
#====================
''''''
#''
#====================
bottles=99
base=''
base+='inventory bottles:'
base+=str(bottles)
print(base)
bottles=99
base=''
base+='inventory bottles:'
base+=str(bottles)
print(base)
#inventory bottles:99
#====================
bottles=99
base+='inventory bottles:'
base+=str(bottles)
print(base)
#inventory bottles:99inventory bottles:99
#====================
str('98.6')
#'98.6'
#====================
str('keerthi')
#'keerthi'
#====================
(9e05)
#900000.0
#====================
9e05
str(9e05)
#'900000.0'
#====================
palindrome='a man,\na plan,\ncanal,\npanama'
palindrome
#'a man,\na plan,\ncanal,\npanama'
#====================
palindrome='a mam\na plan\ncanal\npanama'
print(palindrome)
#a mam
#a plan
#canal
#panama
#====================
print('\nkeerthi')

#keerthi

#====================
print('\tabc')
#	abc
#====================
b\tc
print('\ta\tb\tc')
#	a	b	c
#====================
a\tb
print('a\tb')
#a	b
#====================
abc\t
print('abc\t')
#abc	
#====================
testing='\"hello.\"this is keerthi!\"\"Welcome to TechnoIdentity.\"'
print(testing)
#"hello."this is keerthi!""Welcome to TechnoIdentity."
#====================
testimony
testimony = "\"I did nothing!\" he said. \"Not that either! Or the other thing.\""
print(testimony)
#"I did nothing!" he said. "Not that either! Or the other thing."
#====================
taking="take this one \\ this one"
print(taking)
#take this one \ this one
#====================
speech = 'Today we honor our friend, the backslash: \\.' 
print(speech)
#Today we honor our friend, the backslash: \.
#====================
'Release the kraken! ' + 'At once!'
#'Release the kraken! At once!'
#====================
Combine with +:
==============
a='duck'
b=a
c='grey duck'
a+b+c
#'duckduckgrey duck'
#====================
print(a,b,c)
#duck duck grey duck
#====================
Duplicate with *:
=======================
start='na' *5 +'\n'
middle='hey' *5 +'\n'
end='goodbye'*3
print(start+middle+end)
#nanananana
#heyheyheyheyhey
#goodbyegoodbyegoodbye
#====================
 letters = 'abcdefghijklmnopqrstuvwxyz'
print(letters[0])
print(letters[1])
print(letters[-1])
print(letters[21])
print(letters[25])
#a
#b
#z
#v
#z
#====================
name='keerthi'
print(name[1])
print(name[1-2])
print(name[6])
#e
#i
#i
#====================
name[1]='i'
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-209-b1567eb4c9e7> in <module>()
----> 1 name[1]='i'

TypeError: 'str' object does not support item assignment
#====================
name.replace('e','i')
#'kiirthi'
