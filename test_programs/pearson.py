import unittest as ut
import distance as de

class eculidentest(ut.TestCase):
    def test1(self):
        lst=[43,21,25,42,57,59]
        lst1=[99,65,79,75,87,81]
        self.assertEqual(de.pearson_correlation(lst,lst1), 0.5298089018901743)
    def test2(self):
        lst=[43,21,25,42,57,59]
        lst1=[99,65,79,75,87,81]
        self.assertEqual(de.pearson_correlation(lst,lst1), 0.52980)
    def test3(self):
        lst=[43,21,25,42,57,59]
        lst1=[99,65,79,75,87,81]
        with self.assertRaises(IndexError):
            pearson_correlation(lst,lst1)
    def test4(self):
        lst=[43,21,25,42,57,59]
        lst1=[99,65,79,75,87,81]
        self.assertEqual(de.pearson_correlation(lst,lst1), 0)
    def test5(self):
        lst=[43,21,25,42,57,59]
        lst1=[99,65,79,75,87,81]
        with self.assertRaises(IndexError):
            pearson_correlation(lst,lst1)
if __name__ == '__main__':
    ut.main()