import unittest as ut
import distance as de

class eculidentest(ut.TestCase):
    def test1(self):
        lst=[58,59,60,61,62,63,64,65,66,67,68,69,70,71,72]
        lst1=[115,117,120,123,126,129,132,135,139,142,146,150,154,159,164]
        self.assertEqual(de.eculiden_distance(lst,lst1), 280.8843178249722)
    def test2(self):
        lst=[58,59,60,61,62,63,64,65,66,67,68,69,70,71,72]
        lst1=[115,117,120,123,126,129,132,135,139,142,146,150,154,159,164]
        self.assertEqual(de.eculiden_distance(lst,lst1), 280.88431782497)
    def test3(self):
        lst=[63,64,65,66,67,68,69,70,71,72]
        lst1=[115,117,120,123,126,129,132,135,139,142,146,150,154,159,164]
        with self.assertRaises(IndexError):
            eculiden_distance(lst,lst1)
    def test4(self):
        lst=[58,59,60,61,62,63,64,65,66,67,68,69,70,71,72]
        lst1=[115,117,120,123,126,129,132,135,139,142,146,150,154,159,164]
        self.assertEqual(de.eculiden_distance(lst,lst1), 280)
    def test5(self):
        lst=[58,59,60,6,62,63,64,65,66,67,68,69,70,71,72]
        lst1=[115,117,120,123,126,129,132,135,139,142,146,150,154,159,164]
        with self.assertRaises(IndexError):
            eculiden_distance(lst,lst1)
if __name__ == '__main__':
    ut.main()