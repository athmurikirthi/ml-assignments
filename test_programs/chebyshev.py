import unittest as ut
import distance as de

class chebyshevtest(ut.TestCase):
    def test1(self):
        lst=[43,21,25,42,57,59]
        lst1=[99,65,79,75,87,81]
        self.assertEqual(de.chebyshev_distance(lst,lst1), 56)
    def test2(self):
        lst=[43,21,25,42,57,59]
        lst1=[99,65,79,75,87,81]
        self.assertEqual(de.chebyshev_distance(lst,lst1), 57)
    def test3(self):
        lst=[43,21,25,42]
        lst1=[99,65,79,75,87,81]
        with self.assertRaises(IndexError):
            chebyshev_distance(lst,lst1)
    def test4(self):
        lst=[43,21,25,42,57,59]
        lst1=[99,65,79,75,87,81]
        self.assertEqual(de.chebyshev_distance(lst,lst1), 5)
if __name__ == '__main__':
    ut.main()