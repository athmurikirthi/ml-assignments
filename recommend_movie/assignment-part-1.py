
# coding: utf-8

# # pearson_correlation

# In[1]:

lst=[43,21,25,42,57,59]
lst1=[99,65,79,75,87,81]

def pearson_correlation(lst,lst1):
    ml=[]
    sq=[]
    sq1=[]
    l1=(len(lst))
    for i in range(len(lst)):
        ml.append(lst[i]*lst1[i])
        su=sum(ml)
        s1=sum(lst)
        s2=sum(lst1)
        upt=(su-((s1*s2)/l1))
    for i in lst:
        sq.append(i**2)
        suq=sum(sq)
    for i in lst1:
        sq1.append(i**2)
        suq1=sum(sq1)
        lwp1=suq-((s1**2)/l1)
        lwp2=suq1-((s2**2)/l1)
        p1=lwp1*lwp2
        lwp=p1**(1/2)
        pear_co=upt/lwp
    return pear_co


# In[2]:

pearson_correlation(lst,lst1)


# # read movie rating and names files

# In[3]:

import numpy as np
loc='movie_names.csv'
def movie_names(loc):
    return np.genfromtxt(loc,delimiter=',',dtype='str')


# In[4]:

movie_names(loc)


# In[5]:

loc1='movie_ratings.csv'
def movie_ratings(loc1):
    return np.genfromtxt(loc1,delimiter=',',dtype='str')


# In[6]:

movie_ratings(loc1)


# # normalization for movie ratings

# In[8]:

loc='movie_ratings.csv'
def movie_ratings(loc):
    d1=np.loadtxt(loc,delimiter=',',skiprows=1,usecols=[1,2,3,4,5,6,7])
    return d1
mov_rat=movie_ratings(loc)


# In[9]:

mov_rat


# In[12]:

def movrat_size(mov_rat):
    size=mov_rat.shape
    return size
mrs=movrat_size(mov_rat)


# In[13]:

mrs


# In[15]:

def cls(mov_rat):
    col=[]
    for i in range(mrs[0]):
        for j in range(mrs[1]):
            col.append(mov_rat[:,j])
        break
    return col
all_col=cls(mov_rat)


# In[22]:

alcl=all_col
cl1=alcl[0]
cl2=alcl[1]
cl3=alcl[2]
cl4=alcl[3]
cl5=alcl[4]
cl6=alcl[5]
cl7=alcl[6]


# In[24]:

def normalization_imdb(cl1):
    max=10
    min=0
    return (cl1-min)/(max-min)
c1=normalization_imdb(cl1)


# In[31]:

def normalization_rt(cl2):
    max=10
    min=0
    return (cl2-min)/(max-min)
c2=normalization_rt(cl2)


# In[41]:

def normalization_rv(cl3):
    max=4
    min=0
    return (cl3-min)/(max-min)
c3=normalization_rv(cl3)


# In[43]:

def normalization_re(cl4):
    max=4
    min=0
    return (cl4-min)/(max-min)
c4=normalization_re(cl4)


# In[46]:

def normalization_mc(cl5):
    max=100
    min=0
    return (cl5-min)/(max-min)
c5=normalization_mc(cl5)


# In[49]:

def normalization_tg(cl6):
    max=5
    min=0
    return (cl6-min)/(max-min)
c6=normalization_tg(cl6)


# In[51]:

def normalization_ac(cl7):
    max=5
    min=0
    return (cl7-min)/(max-min)
c7=normalization_ac(cl7)


# In[78]:

combine=np.vstack((c1,c2,c3,c4,c5,c6,c7)).T


# In[79]:

combine


# In[85]:

cv=np.cov(combine)


# # similarity matrixs of normalization of movie ratings

# In[88]:

np.corrcoef(combine)


# # pearson for ac and other ratings

# In[98]:

def imdb_ac(f,c1,c7):
    return (f(c1,c7)


# In[104]:

	def rt_ac(f,c2,c7):
    return (f(c2,c7))


# In[106]:

def rv_ac(f,c3,c7):
    return (f(c3,c7))


# In[108]:

def re_ac(f,c4,c7):
    return (f(c4,c7))


# In[110]:

def mc_ac(f,c5,c7):
    return (f(c5,c7))


# In[113]:

def tg_ac(f,c6,c7):
    return (f(c6,c7))


# # closest reviewers

# ## re and ac are the closest reviwers

# In[88]:

combine


# In[95]:

arr=np.where(combine==0,'nan',combine)


# In[112]:

arr

