
# coding: utf-8

# ### bar chart

# In[28]:

import matplotlib.pyplot as plt
import numpy as np
get_ipython().magic('matplotlib inline')


# In[47]:

x=[690,735,570,609,1350,840,930,555,654,555,1020,690,735,600,375,900,1050,300,840,525,930,1350,480,855,765,1020,900,2640,2400,1350]
y=[36,53,29,53,33,35,37,37,71,37,32,89,53,65,32,38,45,34,35,33,39,34,41,33,37,32,40,35,39,34]
ind=np.arange(len(x))
plt.bar(ind,y,label='dist')
plt.bar(ind,x,label='rent')
plt.xlabel('distance')
plt.ylabel('rent')
plt.title('BAR_CHART')
plt.legend()
plt.show()


# ### histogram

# In[50]:

x=[690,735,570,609,1350,840,930,555,654,555,1020,690,735,600,375,900,1050,300,840,525,930,1350,480,855,765,1020,900,2640,2400,1350]
bins=[0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400]
plt.hist(x,bins,histtype='bar',rwidth=0.8,label='rent')
plt.xlabel('rent')
plt.ylabel('distance')
plt.title('histogram')
plt.legend()
plt.show()


# ### scatter plot

# In[55]:

x=[690,735,570,609,1350,840,930,555,654,555,1020,690,735,600,375,900,1050,300,840,525,930,1350,480,855,765,1020,900,2640,2400,1350]
y=[36,53,29,53,33,35,37,37,71,37,32,89,53,65,32,38,45,34,35,33,39,34,41,33,37,32,40,35,39,34]
plt.scatter(x,y,s=75,marker="*",label='scatter',color='c')
plt.xlabel('rent')
plt.ylabel('distance')
plt.title('scatter plot')
plt.legend()
plt.show()


# ### box plot

# In[60]:

x=[690,735,570,609,1350,840,930,555,654,555,1020,690,735,600,375,900,1050,300,840,525,930,1350,480,855,765,1020,900,2640,2400,1350]
plt.boxplot(x)
plt.ylabel('rent')
plt.title('box plot')
plt.legend()
plt.show()


# In[61]:

a=np.array([99,100,45,63,47])


# In[62]:

b=np.array([12,14])


# In[63]:

a_new=a[:,np.newaxis]


# In[64]:

res=a_new+b


# In[68]:

res


# In[65]:

b_new=b[:,np.newaxis]


# In[66]:

res1=a+b_new


# In[67]:

res1


# In[ ]:



