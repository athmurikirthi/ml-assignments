import numpy as np
import funtions as fs
import unittest as ut

class sort_funtion(ut.TestCase):
    def test1(self):
        lst=np.array([1,3])
        lst1=np.array([2,4])
        lst2=np.sort((lst,lst1),axis=0)
        lst3=np.array([[1,3],[2,4]])
        self.assertEqual(fs.all_fuction1(lst2,lst3),True)
    def test2(self):
        lst=np.array([1,2])
        lst1=np.array([3,4])
        lst2=np.sort((lst,lst1),axis=0)
        lst3=np.array([[1,2],[3,4]])
        self.assertEqual(fs.all_fuction1(lst2,lst3),True)
    def test3(self):
        lst=np.array([1.88,2.99])
        lst1=np.array([3.11,4.11])
        lst2=np.sort((lst,lst1),axis=0)
        lst3=np.array([[1.88,2.99],[3.11,4.11]])
        self.assertAlmostEqual(fs.all_fuction1(lst2,lst3),True)