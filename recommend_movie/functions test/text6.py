import numpy as np
import funtions as fs
import unittest as ut

class concatenate_funtion(ut.TestCase):
    def test1(self):
        lst=np.array([1,2])
        lst1=np.array([3,4])
        lst2=np.concatenate((lst,lst1),axis=0)
        lst3=np.array([[1,2,3,4]])
        self.assertEqual(fs.all_fuction1(lst2,lst3),True)
    def test2(self):
        lst=np.array([1,2])
        lst1=np.array([3,4])
        lst2=np.concatenate((lst,lst1),axis=0)
        lst3=np.array([[1,3,4,2]])
        self.assertEqual(fs.all_fuction2(lst2,lst3),False)
    def test3(self):
        lst=np.array([1.11,2.22])
        lst1=np.array([3.33,4.44])
        lst2=np.concatenate((lst,lst1),axis=0)
        lst3=np.array([[1.11,3.33,4.44,2.22]])
        self.assertAlmostEqual(fs.all_fuction2(lst2,lst3),False)