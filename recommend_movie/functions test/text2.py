import unittest as ut
import numpy as np
import funtions as fs


class where_funtion(ut.TestCase):
    def test1(self):
        lst=np.arange(1,6)
        lst=np.where(lst>1,0,lst)
        lst1=[1,0,0,0,0]
        self.assertEqual(fs.any_fuction1(lst,lst1),True)
    def test2(self):
        lst=np.arange(1,6)
        lst=np.where(lst>1,0,lst)
        lst1=[1,0,0,1,1]
        self.assertEqual(fs.any_fuction2(lst,lst1),True)
    def test3(self):
        lst=np.array([1.01,20.1,3.0,4.12,5.62])
        lst=np.where(lst>4.01,0,lst)
        lst1=[ 1.01,0.  ,3.  ,0.  ,0.  ]
        self.assertAlmostEqual(fs.any_fuction2(lst,lst1),False)