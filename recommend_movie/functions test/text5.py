import numpy as np
import funtions as fs
import unittest as ut

class stack_funtion(ut.TestCase):
    def test1(self):
        lst=np.array([1,2])
        lst1=np.array([3,4])
        lst2=np.stack((lst,lst1),axis=1)
        lst3=np.array([[1,3],[2,4]])
        self.assertEqual(fs.all_fuction1(lst2,lst3),True)
    def test2(self):
        lst=np.array([1,2])
        lst1=np.array([3,4])
        lst2=np.stack((lst,lst1),axis=1)
        lst3=np.array([[3,4],[1,2]])
        self.assertEqual(fs.all_fuction2(lst2,lst3),True)
    def test3(self):
        lst=np.array([1.01,2.01])
        lst1=np.array([3.23,4.23])
        lst2=np.stack((lst,lst1),axis=1)
        lst3=np.array([[3.23,4.23],[1.01,2.01]])
        self.assertAlmostEqual(fs.all_fuction2(lst2,lst3),True)