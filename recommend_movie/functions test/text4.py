import unittest as ut
import funtions as fs
import numpy as np

class all_function(ut.TestCase):
    def test1(self):
        lst=np.array([1,2,3])
        lst1=np.array([1,2,3])
        self.assertEqual(fs.all_fuction1(lst,lst1),True)
    def test2(self):
        lst=np.array([1,2,3])
        lst1=np.array([1,2,4])
        self.assertEqual(fs.all_fuction2(lst,lst1),False)
    def test3(self):
        lst=np.array([1,2,3])
        lst1=np.array([4,5,6])
        self.assertEqual(fs.all_fuction2(lst,lst1),True)
    def test4(self):
        lst=np.array([1,2,3])
        lst1=np.array([1,9,4])
        self.assertEqual(fs.all_fuction2(lst,lst1),False)
    def test4(self):
        lst=np.array([10.1,2.01,3.0])
        lst1=np.array([10,2.01,3.0])
        self.assertAlmostEqual(fs.all_fuction2(lst,lst1),False)

