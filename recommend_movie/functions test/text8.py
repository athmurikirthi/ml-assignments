import numpy as np
import funtions as fs
import unittest as ut

class stack_funtion(ut.TestCase):
    def test1(self):
        lst=np.array([1,2,3,4])
        lst=lst[:-1]
        lst1=[1,2,3]
        self.assertEqual(fs.all_fuction1(lst,lst1),True)
    def test2(self):
        lst=np.array([1,2,3,4])
        for i in range(len(lst)):
            return lst[-i-1]
        lst1=[4,3,2,1]
        self.assertEqual(fs.all_fuction1(lst,lst1),True)
    def test3(self):
        lst=np.array([1,2,3,4,5])
        lst=lst[np.array([2,3])]
        lst1=[3,4]
        self.assertEqual(fs.all_fuction1(lst,lst1),True)
    def test4(self):
        lst=np.array([1.22,2.33,3.01,4.52,5.89])
        lst=lst[np.array([2,3])]
        lst1=[3.01,4.52]
        self.assertEqual(fs.all_fuction1(lst,lst1),True)
    