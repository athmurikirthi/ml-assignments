import unittest as ut
import funtions as fs
import numpy as np

class anytest(ut.TestCase):
    def test1(self):
        lst=np.array([1,2,3])
        lst1=np.array([1,2,3])
        self.assertEqual(fs.any_fuction1(lst,lst1),True)
    def test2(self):
        lst=np.array([1,2,3])
        lst1=np.array([1,2,3])
        self.assertEqual(fs.any_fuction2(lst,lst1),False)
    def test3(self):
        lst=np.array([1,2,3])
        lst1=np.array([1,0,0])
        self.assertEqual(fs.any_fuction1(lst,lst1),True)
    def test4(self):
        lst=np.array([1,2,3])
        lst1=np.array([1,-1,-2])
        self.assertEqual(fs.any_fuction1(lst,lst1),True)
    def test5(self):
        lst=np.array([1,2,3])
        lst1=np.array([1])
        self.assertEqual(fs.any_fuction1(lst,lst1),True)
    def test6(self):
        lst=np.array([1.01,2.01,3.1])
        lst1=np.array([1.01,2,3.0])
        self.assertAlmostEqual(fs.any_fuction1(lst,lst1),True)