import numpy as np
import unittest as ut

def result(a):
    return (a[0]+a[1])+9
class apply_along_axis_function(ut.TestCase):
    def test1(self):
        def apply_along_axis_func(lst):
            lst=np.array([[1,2,3],[4,5,6]])
            lst=np.apply_along_axis(result,0,lst)
            lst1=[14,16,18]
            lst2=np.all(lst==lst1)
            self.assertEqual(lst2,True)
    def test2(self):
        def apply_along_axis_func(lst):
            lst=np.array([[1,2,3],[4,5,6]])
            lst=np.apply_along_axis(result,0,lst)
            lst1=[14,20,18]
            lst2=np.all(lst!=lst1)
            self.assertEqual(lst2,True)
    def test3(self):
        def apply_along_axis_func(lst):
            lst=np.array([[1.41,2.41,3.58],[4.01,5.87,6.1]])
            lst=np.along.apply_along_axis(result,0,lst)
            lst1=[14.42,17.28,18.68]
            lst2=np.all(lst==lst1)
            self.assertAlmostEqual(lst2,True)
            
        